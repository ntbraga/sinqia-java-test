package br.com.sinqia.sinqiajavatest.rest;

import br.com.sinqia.sinqiajavatest.model.location.Coordinates;
import br.com.sinqia.sinqiajavatest.model.payload.*;

import br.com.sinqia.sinqiajavatest.services.PrestadoresService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("prestadores")
public class PrestadoresRestController {

    @Autowired
    private PrestadoresService prestadoresService;

    @GetMapping
    public List<PrestadorResponse> obterPrestadoresSaude(
            @RequestParam(name = "especialidade", required = false) String especialidade,
            @RequestParam(name = "latitude", required = true) Double latitude,
            @RequestParam(name = "longitude", required = true) Double longitude
    ) {
        Coordinates coordinates = Coordinates.builder().latitude(latitude).longitude(longitude).build();
        return this.prestadoresService.getPrestadoresNearBy(especialidade, coordinates);
    }



}
