package br.com.sinqia.sinqiajavatest.model.places;

import lombok.Data;
import lombok.Getter;

import java.util.List;

@Getter
@Data
public class PrestadorSaude {

    private String nome;
    private String endereco;
    private List<String> especialidades;

}
