package br.com.sinqia.sinqiajavatest.model.location;

import com.google.maps.model.LatLng;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Coordinates {

    private final Double latitude;
    private final Double longitude;


    public LatLng toPoint() {
        return new LatLng(this.latitude, this.longitude);
    }

}
