package br.com.sinqia.sinqiajavatest.model.payload;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@Builder
public class PrestadorResponse implements Comparable<PrestadorResponse> {
    private String nome;
    private String endereco;
    private Double latitude;
    private Double longitude;
    private Double distanciaEmKm;

    @Override
    public int compareTo(PrestadorResponse o) {
        if (this.distanciaEmKm != null && o != null && o.distanciaEmKm != null) {
            return this.distanciaEmKm.compareTo(o.distanciaEmKm);
        }
        return -1;
    }
}
