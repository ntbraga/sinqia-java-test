package br.com.sinqia.sinqiajavatest.config;

import br.com.sinqia.sinqiajavatest.model.places.PrestadorSaude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.maps.GeoApiContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

@Configuration
public class ServerConfig {

    @Value("${google.maps}")
    private String mapsKey;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private ObjectMapper mapper;


    @Bean
    public GeoApiContext matrixApi() {
        return new GeoApiContext.Builder().apiKey(this.mapsKey).build();
    }

    @Bean(name = "prestadores")
    public List<PrestadorSaude> getPrestadores() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:static/places.json");

        return this.mapper.readValue(resource.getInputStream(), new TypeReference<List<PrestadorSaude>>() {});
    }

}
