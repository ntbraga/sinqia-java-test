package br.com.sinqia.sinqiajavatest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Locale;
import java.util.TimeZone;

@SpringBootApplication
@EnableAutoConfiguration
public class SinqiaJavaTestApplication {

	public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("America/Sao_Paulo"));
		Locale.setDefault(Locale.forLanguageTag("pt-Br"));
		SpringApplication.run(SinqiaJavaTestApplication.class, args);
	}

}
