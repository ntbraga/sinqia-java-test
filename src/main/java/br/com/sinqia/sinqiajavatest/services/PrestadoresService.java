package br.com.sinqia.sinqiajavatest.services;

import br.com.sinqia.sinqiajavatest.model.location.Coordinates;
import br.com.sinqia.sinqiajavatest.model.payload.PrestadorResponse;
import br.com.sinqia.sinqiajavatest.model.places.PrestadorSaude;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class PrestadoresService {

    @Autowired
    @Qualifier("prestadores")
    private List<PrestadorSaude> prestadores;

    @Autowired
    private GoogleMapsService googleMapsService;

    public List<PrestadorSaude> filterByEspecialidade(String especialidade) {
        if(especialidade == null || "".equals(especialidade)) {
            return prestadores;
        }
        return prestadores
                .stream()
                .filter((prestadorSaude -> prestadorSaude.getEspecialidades().contains(especialidade)))
                .collect(Collectors.toList());
    }

    public List<PrestadorResponse> getPrestadoresNearBy(String especialidade, Coordinates coordinates) {
        if(coordinates == null) {
            return new ArrayList<>();
        }

        LatLng point = coordinates.toPoint();

        List<PrestadorSaude> prestadores = this.filterByEspecialidade(especialidade);

        return prestadores.stream()
                .map((p) -> this.mapPrestadorToResponse(point, p))
                .filter(Objects::nonNull)
                .sorted()
                .collect(Collectors.toList());
    }

    private PrestadorResponse mapPrestadorToResponse(LatLng originPoint, PrestadorSaude prestadorSaude) {
        GeocodingResult[] results = this.googleMapsService.addressToPoint(prestadorSaude.getEndereco());
        if(results == null) {
            return null;
        }

        GeocodingResult result = Arrays.stream(results)
                .filter((geocodingResult -> geocodingResult.geometry != null && geocodingResult.geometry.location != null))
                .findFirst()
                .orElse(null);

        if(result == null) {
            return null;
        }

        LatLng targetPoint = result.geometry.location;

        DistanceMatrixApiRequest request = this.googleMapsService.createRequest();
        request = request.origins(originPoint).destinations(targetPoint);

        Double distance = this.googleMapsService.getDistance(request);

        if(distance == null) {
            return null;
        }

        return PrestadorResponse
                .builder()
                .nome(prestadorSaude.getNome())
                .endereco(prestadorSaude.getEndereco())
                .latitude(targetPoint.lat)
                .longitude(targetPoint.lng)
                .distanciaEmKm(distance)
                .build();
    }

}
