package br.com.sinqia.sinqiajavatest.services;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.stream.Stream;

@Service
public class GoogleMapsService {
    private static final Logger logger = LoggerFactory.getLogger(GoogleMapsService.class);

    @Autowired
    private GeoApiContext geoApiContext;

    public DistanceMatrixApiRequest createRequest() {
        return DistanceMatrixApi.newRequest(this.geoApiContext);
    }

    public Double getDistance(DistanceMatrixApiRequest request) {
        try {
            DistanceMatrix matrix = request
                    .language("pt-BR")
                    .await();

            return Stream.of(matrix.rows)
                    .map((row) -> Stream.of(row.elements)
                            .map((element) -> element.distance.inMeters)
                            .max(Long::compareTo).orElse(0L)).max(Long::compareTo).orElse(0L) / 1000D;
        } catch (ApiException | InterruptedException | IOException ex) {
            logger.error("Erro ao calcular distância.", ex);
            return null;
        }
    }

    public GeocodingResult[] addressToPoint(String address) {
        try {
            return GeocodingApi.geocode(this.geoApiContext, address).await();
        } catch (ApiException | InterruptedException | IOException ex) {
            logger.error("Erro ao geocodificar endereço.", ex);
        }
        return null;
    }


}
